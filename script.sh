#!/bin/bash

echo "#CREATE FILE                                                                                  "  > roles/create/tasks/main.yaml                                                                               "  > roles/reboot/tasks/main.yaml
for (( host=101; host <= 254; host++ ))
do 
echo "- name: Creating machine_$host                                                                " >> roles/create/tasks/main.yaml
echo "  command: pct create 11$host local:vztmpl/ubuntu-16.04-standard_16.04.5-1_amd64.tar.gz         " >> roles/create/tasks/main.yaml
echo "    -arch amd64                                                                               " >> roles/create/tasks/main.yaml 
echo "    -ostype ubuntu                                                                            " >> roles/create/tasks/main.yaml
echo "    -password Wedfyujijh23                                                                    " >> roles/create/tasks/main.yaml
echo "    -nameserver  8.8.8.8                                                                      " >> roles/create/tasks/main.yaml
echo "    -cores 2                                                                                  " >> roles/create/tasks/main.yaml
echo "    -memory 1024                                                                              " >> roles/create/tasks/main.yaml
echo "    -swap 1024                                                                                " >> roles/create/tasks/main.yaml
echo "    -net0 'name=eth0,bridge=vmbr1,gw=10.0.0.1,ip=10.0.0.$host/24'                             " >> roles/create/tasks/main.yaml
#echo "    -ssh-public-keys ~/.ssh/id_rsa_pub                                                       " >> roles/create/tasks/main.yaml
echo "#NEXT NODE                                                                                    " >> roles/create/tasks/main.yaml
done

for (( host=101; host <= 254; host++ ))
do 
#CLONE
echo "- name: Cloning machine_$host                                                                 " >> roles/clone/tasks/main.yaml
echo "  command: pct clone 100 $host                                                                " >> roles/clone/tasks/main.yaml
echo "- name: Setting-up machine_$host                                                              " >> roles/clone/tasks/main.yaml
echo "   command: pct set $host                                                                     " >> roles/clone/tasks/main.yaml      
echo "     -net0 'name=eth0,bridge=vmbr1,gw=10.0.0.1,ip=10.0.0.$host/24'                            " >> roles/clone/tasks/main.yaml  
echo "#NEXT NODE                                                                                    " >> roles/clone/tasks/main.yaml
#STOP
echo "- name: Stoping machine_$host                                                                 " >> roles/stop/tasks/main.yaml
echo "  command: pct stop $host                                                                   " >> roles/stop/tasks/main.yaml
echo "#NEXT NODE                                                                                    " >> roles/stop/tasks/main.yaml
#START
echo "- name: Starting machine_$host                                                                " >> roles/start/tasks/main.yaml
echo "  command: pct start $host                                                                  " >> roles/start/tasks/main.yaml
echo "#NEXT NODE                                                                                    " >> roles/start/tasks/main.yaml
#DESTROY
echo "- name: Destroying machine_$host                                                              " >> roles/destroy/tasks/main.yaml
echo "  command:  pct destroy $host                                                               " >> roles/destroy/tasks/main.yaml
echo "#NEXT NODE                                                                                    " >> roles/destroy/tasks/main.yaml
#REBOT
echo "- name: Rebboting machine_$host                                                               " >> roles/reboot/tasks/main.yaml
echo "  command: pct reboot $host                                                                 " >> roles/reboot/tasks/main.yaml
echo "#NEXT NODE                                                                                    " >> roles/clone/tasks/main.yaml
done
