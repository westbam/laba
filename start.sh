
mkdir -p roles/create/tasks/
mkdir -p roles/start/tasks/
mkdir -p roles/stop/tasks/
mkdir -p roles/destroy/tasks/
mkdir -p roles/reboot/tasks/
mkdir -p group_vars/
mkdir -p play/
#CREATE PROJECT GHOST
echo "#CREATE FILE ZABBIX_HOSTS                                                                     "  > zabbix_hosts.yaml
echo "#CREATE TASK CREATE_CT                                                                        "  > roles/create/tasks/main.yaml
echo "#CREATE TASK START                                                                            "  > roles/start/tasks/main.yaml
echo "#CREATE TASK STOP                                                                             "  > roles/stop/tasks/main.yaml
echo "#CREATE TASK REBOOT                                                                           "  > roles/reboot/tasks/main.yaml
echo "#CREATE TASK CLONE                                                                            "  > roles/clone/tasks/main.yaml
echo "#CREATE FILE INVENTORY                                                                        "  > inventory.ini 

for (( host=10; host <= 99; host++ ))
do 
echo "   -                                                                                          " >> zabbix_hosts.yaml
echo "      host: 10.0.0.$host                                                                      " >> zabbix_hosts.yaml
echo "      name: data-dev$host                                                                     " >> zabbix_hosts.yaml
echo "      groups:                                                                                 " >> zabbix_hosts.yaml
echo "        -                                                                                     " >> zabbix_hosts.yaml
echo "          name: data-dev                                                                      " >> zabbix_hosts.yaml
echo "      inventory_mode: DISABLED                                                                " >> zabbix_hosts.yaml
#ANSIBLE INVENTORY          
echo "machine_$host ansible_host=10.0.0.$host ansible_user=root ansible_password=Wedfyujijh23       " >> inventory.ini
#CREATE ANSIBLE ROLES FOR PROXMOX SETUP
##CLONE
echo "- name: Cloning machine_$host                                                                 " >> roles/clone/tasks/main.yaml
echo "  command: pct clone 100 $host                                                                " >> roles/clone/tasks/main.yaml
echo "- name: Setting-up machine_$host                                                              " >> roles/clone/tasks/main.yaml
echo "   command: pct set $host                                                                     " >> roles/clone/tasks/main.yaml      
echo "     -net0 'name=eth0,bridge=vmbr1,gw=10.0.0.1,ip=10.0.0.$host/24'                            " >> roles/clone/tasks/main.yaml  
echo "#NEXT NODE                                                                                    " >> roles/clone/tasks/main.yaml
##STOP
echo "- name: Stoping machine_$host                                                                 " >> roles/stop/tasks/main.yaml
echo "  command: pct stop $host                                                                     " >> roles/stop/tasks/main.yaml
echo "#NEXT NODE                                                                                    " >> roles/stop/tasks/main.yaml
##START
echo "- name: Starting machine_$host                                                                " >> roles/start/tasks/main.yaml
echo "  command: pct start $host                                                                    " >> roles/start/tasks/main.yaml
echo "#NEXT NODE                                                                                    " >> roles/start/tasks/main.yaml
##DESTROY
echo "- name: Destroying machine_$host                                                              " >> roles/destroy/tasks/main.yaml
echo "  command:  pct destroy $host                                                                 " >> roles/destroy/tasks/main.yaml
echo "#NEXT NODE                                                                                    " >> roles/destroy/tasks/main.yaml
##REBOT
echo "- name: Rebboting machine_$host                                                               " >> roles/reboot/tasks/main.yaml
echo "  command: pct reboot $host                                                                   " >> roles/reboot/tasks/main.yaml
echo "#NEXT NODE                                                                                    " >> roles/reboot/tasks/main.yaml
done

echo "[DATA-DEV]                                                                                    " >> inventory.ini
for (( host=10; host <= 99; host++ ))
do 
echo "machine_$host                                                                                 " >> inventory.ini
done

echo "[DATA-PROD]                                                                                   " >> inventory.ini
for (( host=100; host <= 200; host++ ))
do 
echo "machine_$host                                                                                 " >> inventory.ini
done

echo "[DATA-RESERVE]                                                                                " >> inventory.ini
for (( host=201; host <= 254; host++ ))
do 
echo "machine_$host                                                                                 " >> inventory.ini
done