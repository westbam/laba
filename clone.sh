#!/bin/bash
for (( host=101; host <= 254; host++ ))
do 
echo "hostname                      : machine_$host                                                 "  > host_vars/machine_$host.yaml
echo "file                          : file$host.txt                                                 " >> host_vars/machine_$host.yaml
echo "ip_address                    : 10.0.0.$host/24                                               " >> host_vars/machine_$host.yaml
echo "ip_gateway                    : 10.0.0.1                                                      " >> host_vars/machine_$host.yaml
done